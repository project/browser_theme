<?php

/**
 * @file
 *
 * Provides browser theme settings administrative interface.
 * @ingroup browser_theme
 */

/**
 * Form builder for browser theme settings configuration.
 *
 * @ingroup forms
 */
function browser_theme_configuration() {
  global $_browser_theme;
  $theme = arg(4);
  $theme_path = '';
  $empty_text = t('There are no file suggestions listed here. You can add a file by clicking the green "add (+)" icon above.');
  browser_theme_load_settings($theme);
  if ($theme) {
    $theme_path = '/' . drupal_get_path('theme', $theme) . '/';
  }
  
  if (!$_browser_theme->js_loaded) {
    $settings['browserTheme']['admin'] = array(
      'themeName'     => $_browser_theme->theme['name'],
      'themePath'     => $_browser_theme->theme['path'],
      'emptyText'     => $empty_text,
    );
    drupal_add_js($settings, 'setting');
    $_browser_theme->js_loaded = TRUE;
  }
  $form = array(
    '#theme'              => 'browser_theme_configuration',
    '#tree'               => TRUE,
    '#empty_text'         => $empty_text,
  );
  $form['page'] = array(
    '#type'               => 'checkbox',
    '#title'              => t('Enable page suggestions <div class="icon help">&nbsp;</div>'),
    '#description'        => t('Suggests browser specific template files in the following order <em>(examples use current browser)</em>:'
                             . '<ul><li><strong>page-browser-major-minor.tpl.php</strong><br /><em>page-' . $_browser_theme->browser . '-' . $_browser_theme->major . '-' . $_browser_theme->minor . '.tpl.php</em></li><li><strong>page-browser-major.tpl.php</strong><br /><em>page-' . $_browser_theme->browser . '-' . $_browser_theme->major . '.tpl.php</em></li><li><strong>page-browser.tpl.php</strong><br /><em>page-' . $_browser_theme->browser . '.tpl.php</em></li></ul>  <br /><br /><br />'
                             . 'Rewrite existing page template file suggestions, prepend browser specific suggestions to cloned array, append clone to original suggestions.<br /><br />'
                             . 'Example: <strong>page-node</strong> > <em>page-browser-major-minor-node</em>'),
    '#default_value'      => $_browser_theme->theme['page'],
  );
  $form['node'] = array(
    '#type'               => 'checkbox',
    '#title'              => t('Enable node suggestions <div class="icon help">&nbsp;</div>'),
    '#description'        => t('Suggests browser specific template files in the following order <em>(examples use current browser)</em>:'
                             . '<ul><li><strong>page-browser-major-minor.tpl.php</strong><br /><em>page-' . $_browser_theme->browser . '-' . $_browser_theme->major . '-' . $_browser_theme->minor . '.tpl.php</em></li><li><strong>page-browser-major.tpl.php</strong><br /><em>page-' . $_browser_theme->browser . '-' . $_browser_theme->major . '.tpl.php</em></li><li><strong>page-browser.tpl.php</strong><br /><em>page-' . $_browser_theme->browser . '.tpl.php</em></li></ul>  <br /><br /><br />'
                             . 'Rewrite existing page template file suggestions, prepend browser specific suggestions to cloned array, append clone to original suggestions.<br /><br />'
                             . 'Example: <strong>page-node</strong> > <em>page-browser-major-minor-node</em>'),
    '#default_value'      => $_browser_theme->theme['node'],
  );
  foreach ($_browser_theme->theme['css'] as $key => $file) {
    $form['css']['files'][$key]['weight'] = array(
      '#type'               => 'weight',
      '#delta'              => count($_browser_theme->theme['css']),
      '#default_value'      => $key,
      '#attributes'         => array('class' => 'weight'),
    );
    $form['css']['files'][$key]['file'] = array(
      '#type'               => 'textfield',
      '#size'               => 25,
      '#field_prefix'       => $theme_path,
      '#value'              => $_browser_theme->theme['css'][$key]['file'],
      '#attributes'         => array('class' => 'file'),
    );
    $form['css']['files'][$key]['preprocess'] = array(
      '#type'               => 'checkbox',
      '#default_value'      => $_browser_theme->theme['css'][$key]['preprocess'],
      '#attributes'         => array('class' => 'preprocess'),
    );
  }
  
  
  foreach ($_browser_theme->theme['js'] as $key => $file) {
    $form['js']['files'][$key]['weight'] = array(
      '#type'               => 'weight',
      '#delta'              => count($_browser_theme->theme['js']),
      '#default_value'      => $key,
      '#attributes'         => array('class' => 'weight'),
    );
    $form['js']['files'][$key]['file'] = array(
      '#type'               => 'textfield',
      '#size'               => 25,
      '#field_prefix'       => $theme_path,
      '#value'              => $_browser_theme->theme['js'][$key]['file'],
      '#attributes'         => array('class' => 'file'),
    );
    $form['js']['files'][$key]['cache'] = array(
      '#type'               => 'checkbox',
      '#default_value'      => $_browser_theme->theme['js'][$key]['cache'],
      '#attributes'         => array('class' => 'cache'),
    );
    $form['js']['files'][$key]['preprocess'] = array(
      '#type'               => 'checkbox',
      '#default_value'      => $_browser_theme->theme['js'][$key]['preprocess'],
      '#attributes'         => array('class' => 'preprocess'),
    );
  }
  return $form;
}
