<?php

/**
 * @file
 *
 * Default theme implementation for administration form.
 * @ingroup browser_theme
 */

/**
 * Formats administration page callback.
 *
 * @ingroup themeable
 */
function theme_browser_theme_configuration($form) {
  $output = '<div id="browser_theme">';
  $output .= '<div class="messages"></div>';
  $output .= drupal_render($form['page']);
  $output .= drupal_render($form['node']);  
  
  // CSS Table
  drupal_add_tabledrag('cssTable', 'order', 'sibling', 'weight');
  $header = array(
    array('data' => '<span class="form-item"><label>Cascading Style Sheet Files &nbsp; <span class="css-files icon help"></span></label><span class="description">' .
    t('Path to folder containing CSS files inside the current theme directory. It is recommended you keep your CSS files inside a separate folder such as: "css".') .
    '</span></span>'),
    array('data' => t('Preprocess'), 'style' => 'text-align: center', 'width' => '100'),
    array('data' => t('Weight')),
    array('data' => '<span class="css-files icon add" title="Add CSS File"></span>', 'class' => 'action'),
  );
  $rows = array();
  $css = &$form['css']['files'];
  foreach (element_children($css) as $key) {
    $row = array();
    $row[] = array('data' => drupal_render($css[$key]['file']), 'class' => 'file');
    $row[] = array('data' => drupal_render($css[$key]['preprocess']), 'width' => '100', 'align' => 'center');
    $row[] = drupal_render($css[$key]['weight']);
    $row[] = array('data' => '<span class="css-files icon delete"></span>', 'class' => 'action');
    $rows[] = array('data' => $row, 'class' => 'draggable');
  }
  if (empty($css)) {
    $row = array();
    $row[] = array('data' => $form['#empty_text'], 'class' => 'file');
    $row[] = array('data' => '<span></span>', 'width' => '100', 'align' => 'center');
    $row[] = array('data' => '<span class="weight"></span>');
    $row[] = array('data' => '<span></span>', 'class' => 'action');
    $rows[] = array('data' => $row, 'class' => 'empty');
  }
  $output .= theme('table', $header, $rows, array('id' => 'cssTable'));
  
  
  // JS Table
  drupal_add_tabledrag('jsTable', 'order', 'sibling', 'weight');
  $header = array(
    array('data' => '<span class="form-item"><label>JavaScript Files &nbsp; <span class="css-files icon help"></span></label><span class="description">' .
    t('Path to folder containing CSS files inside the current theme directory. It is recommended you keep your CSS files inside a separate folder such as: "css".') .
    '</span></span>'),
    array('data' => t('Cache'), 'style' => 'text-align: center', 'width' => '100'),
    array('data' => t('Preprocess'), 'style' => 'text-align: center', 'width' => '100'),
    array('data' => t('Weight')),
    array('data' => '<span class="css-files icon add" title="Add CSS File"></span>', 'class' => 'action'),
  );
  $rows = array();
  $js = &$form['js']['files'];
  foreach (element_children($js) as $key) {
    $row = array();
    $row[] = array('data' => drupal_render($js[$key]['file']), 'class' => 'file');
    $row[] = array('data' => drupal_render($js[$key]['cache']), 'width' => '100', 'align' => 'center');
    $row[] = array('data' => drupal_render($js[$key]['preprocess']), 'width' => '100', 'align' => 'center');
    $row[] = drupal_render($js[$key]['weight']);
    $row[] = array('data' => '<span class="js-files icon delete"></span>', 'class' => 'action');
    $rows[] = array('data' => $row, 'class' => 'draggable');
  }
  if (empty($js)) {
    $row = array();
    $row[] = array('data' => $form['#empty_text'], 'class' => 'file');
    $row[] = array('data' => '<span></span>', 'width' => '100', 'align' => 'center');
    $row[] = array('data' => '<span></span>', 'width' => '100', 'align' => 'center');
    $row[] = array('data' => '<span class="weight"></span>');
    $row[] = array('data' => '<span></span>', 'class' => 'action');
    $rows[] = array('data' => $row, 'class' => 'empty');
  }
  $output .= theme('table', $header, $rows, array('id' => 'jsTable'));
  $output .= '</div>';
  return $output;
}
