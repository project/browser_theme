<?php

/**
 * @file
 *
 * Core class, functions and variables for browser_theme.
 * @ingroup browser_theme
 */

//Initialize browser_theme class and load variables
global $_browser_theme;
$_browser_theme = new stdClass;
$_browser_theme->module['path'] = drupal_get_path('module', 'browser_theme');
browser_theme_load_settings();

// Browscap variables
if (!empty($_SESSION['browscap'])) {
  $_browser_theme->browscap = $_SESSION['browscap'];
}
else {
  $_SESSION['browscap'] = browscap_get_browser($_SERVER['HTTP_USER_AGENT']);
  $_browser_theme->browscap = $_SESSION['browscap'];
}
$_browser_theme->browser = drupal_strtolower($_browser_theme->browscap['browser']);
$_browser_theme->major = $_browser_theme->browscap['majorver'];
$_browser_theme->minor = $_browser_theme->browscap['minorver'];

function browser_theme_load_settings($theme = NULL) {
  global $_browser_theme;
  $defaults = array(
    'name' => NULL,
    'path' => NULL,
    'page' => 1,
    'node' => 1,
    'css'  => array(),
    'js'   => array(),
  );
  // Load the site's default global browser theme settings if it exists. If it does not exist, use Browser Theme's default settings instead.
  $global = variable_get('browser_theme_settings', $defaults);
  $_browser_theme->theme = $global;
  if (!empty($theme)) {
    // Load a specific theme's browser theme settings if it exists. If it does not exist, use the existing global variables instead.
    $_browser_theme->theme = variable_get('browser_theme_' . $theme . '_settings', $_browser_theme->theme);
    $_browser_theme->theme['name'] = $theme;
    $_browser_theme->theme['path'] = drupal_get_path('theme', $theme);
  }
}

function browser_theme_save_settings($theme = NULL) {
  global $_browser_theme;
  if (!empty($theme)) {
    variable_set('browser_theme_' . $theme . '_settings', $_browser_theme->theme);
  }
  else {
    variable_set('browser_theme_settings', $_browser_theme->theme);
  }
}

function _browser_theme_file_suggestions($repository = array()) {
  global $_browser_theme, $theme_key;
  $files = array();
  if (!empty($repository)) {
    foreach ($repository as $key => $file) {
      $info = pathinfo($file['file']);
      $filename = $info['basename'];
      $filepath = $info['dirname'] == '.' ? '' : $info['dirname'] . '/';
      $theme_path = $_browser_theme->theme['path'] ? $_browser_theme->theme['path'] : drupal_get_path('theme', $theme_key);
      $suggestions = array();
      $suggestions[] = $theme_path . '/' . $filepath . $_browser_theme->browser . '-' . $_browser_theme->major . '-' . $_browser_theme->minor . '-' . $filename;
      $suggestions[] = $theme_path . '/' . $filepath . $_browser_theme->browser . '-' . $_browser_theme->major . '-' . $filename;
      $suggestions[] = $theme_path . '/' . $filepath . $_browser_theme->browser . '-' . $filename;
      $suggestions[] = $theme_path . '/' . $filepath . $filename;
      foreach ($suggestions as $key => $path) {
        if (file_exists($path)) {
          $cache = array();
          $preprocess = array();
          if (isset($file['cache'])) {
            $cache = array('cache' => (bool) $file['cache']);
          }
          if (isset($file['preprocess'])) {
            $preprocess = array('preprocess' => (bool) $file['preprocess']);
          }
          $files[] = array_merge(array('path' => $path), $cache, $preprocess);
          break;
        }
      }
    }
  }
  return $files;
}

function _browser_theme_add_css($css = NULL) {
  global $_browser_theme, $theme_key;
  browser_theme_load_settings($theme_key);
  if (!isset($css)) {
    $css = drupal_add_css();
  }
  $files = _browser_theme_file_suggestions($_browser_theme->theme['css']);
  foreach ($files as $key => $file) {
    $css['all']['theme'][$file['path']] = $file['preprocess'];
  }
  return $css;
}

function _browser_theme_add_js($javascript = NULL) {
  global $_browser_theme, $theme_key;
  browser_theme_load_settings($theme_key);
  if (!isset($javascript)) {
    $javascript = drupal_add_js(NULL, NULL, 'header');
  }
  $files = _browser_theme_file_suggestions($_browser_theme->theme['js']);
  foreach ($files as $key => $file) {
    $javascript['theme'][$file['path']] = array('cache' => $file['cache'], 'defer' => FALSE, 'preprocess' => (!$file['cache'] ? FALSE : $file['preprocess']));
  }
  // Browser Theme JavaScript Settings
  $settings = array();
  foreach ($javascript['setting'] as $key => $setting) {
    foreach ($setting as $name => $array) {
      switch ($name) {
        case 'browserTheme':
          $settings = $array;
          unset($javascript['setting'][$key]);
          break;
      }
    }
  }
  $javascript['setting'][] = array('browserTheme' => array_merge_recursive(array(
    'modulePath'    => $_browser_theme->module['path'],
    'themeName'     => $_browser_theme->theme['name'],
    'themePath'     => $_browser_theme->theme['path'],
  ), $settings));
  return $javascript;
}

function _browser_theme_ajax_save() {
  global $_browser_theme;
  $_browser_theme->theme = array();
  $_browser_theme->theme['name'] = $_POST['themeName'];
  $_browser_theme->theme['path'] = $_POST['themePath'];
  $_browser_theme->theme['page'] = (int)$_POST['page'];
  $_browser_theme->theme['node'] = (int)$_POST['node'];
  $css_count = (int)($_POST['css'] - 1);
  $js_count = (int)($_POST['js'] - 1);
  $css_file = json_decode($_POST['cssFile'], TRUE);
  $css_preprocess = json_decode($_POST['cssPreprocess'], TRUE);
  $js_file = json_decode($_POST['jsFile'], TRUE);
  $js_cache = json_decode($_POST['jsCache'], TRUE);
  $js_preprocess = json_decode($_POST['jsPreprocess'], TRUE);
  $_browser_theme->theme['css'] = array();
  $_browser_theme->theme['js'] = array();
  for ($i = 0; $i <= $css_count; $i++) {
    $_browser_theme->theme['css'][$i]['file'] = $css_file[$i];
    $_browser_theme->theme['css'][$i]['preprocess'] = $css_preprocess[$i];
  }
  for ($i = 0; $i <= $js_count; $i++) {
    $_browser_theme->theme['js'][$i]['file'] = $js_file[$i];
    $_browser_theme->theme['js'][$i]['cache'] = $js_cache[$i];
    $_browser_theme->theme['js'][$i]['preprocess'] = $js_preprocess[$i];
  }
  browser_theme_save_settings($_browser_theme->theme['name']);
  exit();
}
