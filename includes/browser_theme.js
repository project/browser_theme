
// Determine if a checkbox is checked or not.
$.fn.isChecked = function(){
  if ($(this).attr('type') === 'checkbox') {
    if ($(this).is(':checked')) {
      return '1';
    }
    else {
      return '0';
    }
  }
};

if(Drupal.tableDrag) {
  // Allows a row to be added then resorts the weight.
  Drupal.tableDrag.prototype.addRow = function() {
    var self = this;
    var table = this.table;
    var tableId = $(table).attr('id');
    var themePath = Drupal.settings.browserTheme.admin.themePath;
    if (themePath === null || themePath === '') { themePath = '' } else { themePath = '/' + themePath + '/'};
    var tbody = $(table).find('tbody');
    var empty = $(tbody).find('tr.empty');
    if (empty.length > 0) {
      empty.remove();
    }
    var tr = $('<tr class="odd"></tr>');
    var td = new Array();
    // Create row templates
    switch(tableId) {
      case 'cssTable':   // CSS Row Template
        td.push('<td class="file"><div class="form-item"><span class="field-prefix">' + themePath + '</span> <input type="text" class="form-text file" value="" size="25" maxlength="128"></div></td>');
        td.push('<td align="center"><div class="form-item"><input type="checkbox" class="form-checkbox preprocess" value="1"></div></td>');
        td.push('<td style="display: none;"><div class="form-item"><select class="form-select weight"><option selected="selected" value="0">0</option></select></div></td>');
        td.push('<td class="action"><span class="css-files icon delete"></span></td>');
        break;
      case 'jsTable':
        td.push('<td class="file"><div class="form-item"><span class="field-prefix">' + themePath + '</span> <input type="text" class="form-text file" value="" size="25" maxlength="128"></div></td>');
        td.push('<td align="center"><div class="form-item"><input type="checkbox" class="form-checkbox cache" value="1"></div></td>');
        td.push('<td align="center"><div class="form-item"><input type="checkbox" class="form-checkbox preprocess" value="1"></div></td>');
        td.push('<td style="display: none;"><div class="form-item"><select class="form-select weight"><option selected="selected" value="0">0</option></select></div></td>');
        td.push('<td class="action"><span class="js-files icon delete"></span></td>');
        break;
    }
    $(tr).append($(td.join('')));
    $(tbody).append(tr);
    $('tr:not(".draggable")', tbody).each(function() {
      var row = this;
      self.makeDraggable(row);
      $(row).addClass('draggable');
      $(row).find('td.action').find('span.delete').click(function(){
        self.removeRow(row);
      });          
    });
    this.resortWeight();
  }
  
  // Allows a row to be removed then resorts the weight.
  Drupal.tableDrag.prototype.removeRow = function(row) {
    var self = this;
    var table = this.table;
    var tbody = $(table).find('tbody');
    $(row).remove();
    this.resortWeight();
    if (this.scrollSettings.amount == 0) {
      var tr = $('<tr class="empty odd"></tr>');
      var td = $('<td colspan="5">' + Drupal.settings.browserTheme.admin.emptyText + '</td>');
      $(tr).append(td);
      $(tbody).append(tr);
    }
  }
  
  // Allows the table's row weights to be resorted when a row is added or deleted.
  Drupal.tableDrag.prototype.resortWeight = function() {
    var self = this;
    var table = this.table;
    var tbody = $(table).find('tbody');
    var weights = new Array();
    var rowCount = 0;
    $(tbody).find('tr').each(function(){
      rowCount++;
    });
    this.scrollSettings.amount = rowCount;
    $(tbody).find('tr').each(function(){
      var weight = $(this).find('.' + self.tableSettings.weight[0].target);
      var weightLow = new Array();
      var weightHigh = new Array();
      var weightZero = new Array();
      weightZero.push(0);
      for (i = 0; i < rowCount; i++) {
        weightLow.push(parseInt('-' + (rowCount - i)));
        weightHigh.push(parseInt(i + 1));
      }
      weights = weightLow.concat(weightZero, weightHigh);
      $(weight).empty();
      for (i = 0; i < weights.length; i++) {
        $(weight).append('<option value="' + weights[i] + '">' + weights[i] + '</option>');
      }
    });
    var maxVal = weights[weights.length - 1];
    $(tbody).find('tr').each(function() {
      var weight = $(this).find('.' + self.tableSettings.weight[0].target);
      if (weights.length > 0) {
        $(weight).val(weights.shift());
      }
      else {
        $(weight).val(maxVal);
      }
    });
    this.restripeTable();
  }
}

Drupal.behaviors.browser_theme = function (context) {
  // jGrowl Defaults
  $.jGrowl.defaults.closer = false;
  
  var themeName = Drupal.settings.browserTheme.admin.themeName;
  var themePath = Drupal.settings.browserTheme.admin.themePath;
  var divMessages = $("#browser_theme div.messages");
  
  var form = $('form#system-theme-settings');
  var ajaxFinished = false;
  
  // Form submit event handler
  $(form).submit(function(){
    // Save browser theme settings using ajax and continue submitting the rest of the form.
    if (ajaxFinished) {
      return true;
    }
    
    var page           = $('#browser_theme #edit-page').isChecked();
    var node           = $('#browser_theme #edit-node').isChecked();
    var css            = 0;
    var cssFile        = new Array();
    var cssPreprocess  = new Array();
    var js             = 0;
    var jsFile         = new Array();
    var jsCache        = new Array();
    var jsPreprocess   = new Array();
    $('#browser_theme #cssTable').find('tbody').find('tr:not(".empty")').each(function(){
      var file = $(this).find('input.file').val();
      if (file) {
        cssFile.push('"' + css + '": "' + file + '"');
        cssPreprocess.push('"' + css + '": "' + $(this).find('input.preprocess').isChecked() + '"');
      }
      else {
        var tableId = $(this).closest('table').attr('id');
        Drupal.tableDrag[tableId].removeRow(this);
        css--;
      }
      css++;
    });
    $('#browser_theme #jsTable').find('tbody').find('tr:not(".empty")').each(function(){
      var file = $(this).find('input.file').val();
      if (file) {
        jsFile.push('"' + js + '": "' + $(this).find('input.file').val() + '"');
        jsCache.push('"' + js + '": "' + $(this).find('input.cache').isChecked() + '"');
        jsPreprocess.push('"' + js + '": "' + $(this).find('input.preprocess').isChecked() + '"');
      }
      else {
        var tableId = $(this).closest('table').attr('id');
        Drupal.tableDrag[tableId].removeRow(this);
        js--;
      }
      js++;
    });
    $.ajax({
      url: '/browser_theme/save',
      type: 'POST',
      cache: false,
      data: ({
      'themeName'     : themeName ? themeName : '',
      'themePath'     : themePath ? themePath : '',
      'page'          : page,
      'node'          : node,
      'css'           : css,
      'cssFile'       : '{' + cssFile.join(', ') + '}',
      'cssPreprocess' : '{' + cssPreprocess.join(', ') + '}',
      'js'            : js,
      'jsFile'        : '{' + jsFile.join(', ') + '}',
      'jsCache'       : '{' + jsCache.join(', ') + '}',
      'jsPreprocess'  : '{' + jsPreprocess.join(', ') + '}',
      }),
      success: function(data){
        ajaxFinished = true;
        form.submit();
      }
    });
    return false;
  });
  
  // Add File
  $('th.action span.add').each(function(){
    $(this).click(function(){
      var tableId = $(this).closest('table').attr('id');
      Drupal.tableDrag[tableId].addRow();
    });
  });
  
  // Delete File
  $('td.action span.delete').each(function(){
    $(this).click(function(){
      var tableId = $(this).closest('table').attr('id');
      Drupal.tableDrag[tableId].removeRow($(this).closest('tr'));
    });
  });
  
  // Display Help
  $('.form-item').find('.help').each(function(){
    $(this).click(function(){
      var section = $(this).closest('fieldset').find('legend').text();
      var setting = $(this).closest('.form-item').find('label').text().replace(':','');
      var title = (section ? section + ': ' : '') + setting;
      var description = $(this).closest('.form-item').find('.description').html();
      $(divMessages).jGrowl(description, {
        header: '<div class="icon help"></div>' + title,
        sticky: true
      });
    });
  });

}
