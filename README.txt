
Welcome to Browser Theming Settings! This module is now in beta testing.

If you're having trouble installing this module, please ensure that your 
tar program is not flattening the directory tree, truncating filenames
or losing files.

Installing Browser Theming Settings:
 1. Place the entirety of this directory in sites/all/modules/browser_theme
 2. Enable Browser Theme Settings by navigating to Administer >> Build >> Modules.
 3. Configure Browser Theme Settings by navigating to Administer >> Build >> Themes.

Required modules for use with Browser Theming:
*Browscap
